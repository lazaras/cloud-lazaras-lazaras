
"""
This class acts much like a java interface or a C++ abstract class or a rust trait
in that it doesn't have any functionality of its own but provides a unified call semeantic for 
adding and retrieving data from the sqlite database
"""
class Model:
    def select(self):
        pass

    def insert(self, org, types, location, hours, number, review):
        pass

