from datetime import date
from .Model import Model
import sqlite3
DB_FILE = 'entries.db'    # file for our Database

"""
This class is the implementation of the Model class and handles actually interacting with the sqlite
database, and does so through the select, and insert methods
"""
class model(Model):
    def __init__(self):
        # Make sure our database exists
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        """
        Makes sure the database actually exists before trying to perform operations on it
        """
        try:
            cursor.execute("select count(rowid) from organizations")
        except sqlite3.OperationalError:
            cursor.execute("create table organizations (organization text, types text, location text, hours text, number text, review text)")
        cursor.close()

    def select(self):
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM organizations")
        return cursor.fetchall()

    def insert(self, org, types, location, hours, number, review):
        """
        ensures that the correct data is inserted into the correct rows of the table
        """
        params = {'organization':org, 'types':types, 'location':location, 'hours':hours, 'number':number, 'review':review}
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("insert into organizations (organization, types, location, hours, number, review) VALUES (:organization, :types, :location, :hours, :number, :review)", params)

        connection.commit()
        cursor.close()
        return True
