from flask import redirect, request, url_for, render_template
from flask.views import MethodView
import model

"""
This class extends the MethodView that flask provides, and allows for both handling of GET requests
so the user has access to the web form, and the process of post requests that is used to enter
data user has put into the web form into the database backend
"""
class Add(MethodView):
    def get(self):
        return render_template('add.html')

    def post(self):
        abstract = model.get_model()
        abstract.insert(request.form['organization'], request.form['types'], request.form['location'], request.form['hours'], request.form['number'], request.form['review'])
        return redirect(url_for('index'))
