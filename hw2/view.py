from flask import render_template
from flask.views import MethodView
import model

"""
Primary view class that passes view.html fully rendered to the user's web browser
supplying on request the contents of the database as they currently are through the use of the entires
argument passed to the render_template function
"""
class View(MethodView):
    def get(self):
        abstract = model.get_model()
        entries = [dict(organization=row[0], types=row[1], location=row[2], hours=row[3], number=row[4], review=row[5]) for row in abstract.select()]
        return render_template('view.html',entries=entries)