"""
A simple guestbook flask app.
"""
import flask
from flask.views import MethodView
from index import Index
from add import Add
from view import View

"""
initiates the web server
"""
app = flask.Flask(__name__)       # our Flask app

"""
This route is the main home page that serves requests to both /index.html and / (or domain name with no subpath)
"""
app.add_url_rule('/',
                 view_func=Index.as_view('index'),
                 methods=["GET"])

"""
This is the view route which is responsile for displaying the contents of the database
"""
app.add_url_rule('/view', view_func=View.as_view('view'), methods=["GET"])

"""
This route is reponsible for sending the HTML form, and interting into the sqlite database 
each piece of data entered by the user on the web form
"""
app.add_url_rule('/add',
                 view_func=Add.as_view('add'),
                 methods=['GET', 'POST'])

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
