from flask import render_template
from flask.views import MethodView
import model

"""
This is the simplest of the classes provided by this application and purely returns the landing page
fully rendered
"""
class Index(MethodView):
    def get(self):
        return render_template('index.html')
