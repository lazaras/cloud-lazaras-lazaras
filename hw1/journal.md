# Homework 1 Journal

## Wireshark ping
for whatever reason www.google.com wasn't being resolved with ping so I used 8.8.8.8 which was owned by IANA
and the host I was sending from was AsRock as I was using my desk machine (its a linux compute I regularly use for network debug as part of the CAT)

## Why instance-1 and instance-2 can communicate
(instance 2 is redundant but performed same test with 3)
This was possible because of the virtual switch supplied by google cloud forwarding packets between zones.

## Why instance-1 can't ping instance 4/5
This is because instance-1 has no route to instance-4 because they are on different virtual networks, and aren't linked to each other and no packets are forwarded between them on the local interface.


